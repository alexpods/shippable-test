const http = require('http')
const app = require('./src/app')

const HOST = '0.0.0.0'
const PORT = 80

const server = http.createServer(app)

server.listen(PORT, HOST)
  .on('error', (error) => {
    throw error
  })
  .on('listening', () => {
    console.log(`Listening on ${server.address().port}`)
  })
