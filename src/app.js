const express = require('express')
const bodyParser = require('body-parser')
const cookieSession = require('cookie-session')

const routes    = require('./routes/index.js')
const passport  = require('./services/passport')
const validator = require('express-validator')

const app = express()

app.use(bodyParser.json())
app.use(cookieSession({ httpOnly: true, secret: 'xxx' }))
app.use(passport.initialize())
app.use(validator())

app.use('/', routes)

// General error handling
app.use((err, req, res, next) => {
  console.log(err.stack);
  return res.status(err.status || 500).send(err.message);
})

process.on('unhandledRejection', (error) => {
  console.log(error.stack)
})

module.exports = app