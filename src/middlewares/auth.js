const _ = require('lodash')
const jsonwebtoken = require('jsonwebtoken')
const passport = require('../services/passport')

function clearQueryInRequest(req) {
  Object.defineProperty(req, 'query', { value: {} })
}

function patchQueryInRequest(req, params) {
  const query = req.query

  for (var param of params) {
    query[param] = req.body[_.camelCase(param)]
  }

  Object.defineProperty(req, 'query', {
    value: query
  })
}

function createOauthRedirectMiddleware(oauthVersion, serviceName) {
  const middleware = passport.authenticate(serviceName)

  return function(req, res, next) {
    clearQueryInRequest(req)

    return middleware(req, res, next)
  }
}

function createOauthMiddleware(oauthVersion, serviceName) {
  var params

  const middleware = passport.authenticate(serviceName)

  switch (oauthVersion) {
    case 1:
      params = ['oauth_token', 'oauth_verifier']
      break
    case 2:
      params = ['code', 'state']
      break
  }

  return function oauthMiddleware(req, res, next) {
    patchQueryInRequest(req, params)

    for (var param of params) {
      if (!req.query[param]) {
        return res.status(400).send()
      }
    }

    return middleware(req, res,  (err) => {
      if (
        (err && err.name.includes('TokenError'))
        ||
        (err && err.message.includes('Invalid'))
      ) {
        return res.status(403).send()
      }

      return next(err)
    })
  }
}

module.exports = {
  redirectToFacebook:  createOauthRedirectMiddleware(2, 'facebook'),
  redirectToVkontakte: createOauthRedirectMiddleware(2, 'vkontakte'),
  redirectToGoogle:    createOauthRedirectMiddleware(2, 'google'),
  redirectToTwitter:   createOauthRedirectMiddleware(1, 'twitter'),

  local:      passport.authenticate('local'),
  facebook:   createOauthMiddleware(2, 'facebook'),
  vkontakte:  createOauthMiddleware(2, 'vkontakte'),
  google:     createOauthMiddleware(2, 'google'),
  twitter:    createOauthMiddleware(1, 'twitter'),

  returnToken: async (req, res) => {
    const token = await jsonwebtoken.sign({ id: req.user.id }, 'xxx')

    return res.status(200).send({ token })
  },
}
