const users = require('../services/users')
const wrap = require('async-middleware').wrap

module.exports = exports = {

  get: wrap(async (req, res) => {
    const userId = req.params.userId

    const user = await users.getById(userId)

    if (!user) {
      return res.status(404).send()
    }

    return res.status(200).send(users.toData(user))
  }),

  sanitize:  (req, res, next) => {
    req.sanitize('name').trim()
    req.sanitize('email').trim()

    return next()
  },

  validate: (req, res, next) => {
    req.check('name',     'empty').notEmpty()
    req.check('email',    'empty').notEmpty()
    req.check('email',    'incorrect_email').isEmail()
    req.check('password', 'empty').notEmpty()

    return next()
  },

  create: wrap(async (req, res) => {

    const errors = await req.getValidationResult()

    if (!errors.isEmpty()) {
      return res.status(422).send({ errors: errors.useFirstErrorOnly().array() })
    }

    const name     = req.body.name
    const email    = req.body.email
    const password = req.body.password

    const newUser = await users.createUsingCredentials(name, email, password)

    return res.status(200).send(users.toData(newUser))
  })
}
