const Sequelize = require('sequelize')
const _ = require('lodash')
const sequelize = require('../services/sequelize')

module.exports = sequelize.define('User', {
  id: {
    type: Sequelize.BIGINT,
    field: 'id',
    autoIncrement: true,
    primaryKey: true,
  },
  name: {
    type: Sequelize.STRING(32),
    field: 'name',
    allowNull: false,
    validate: {
      notEmpty: true,
      len: [1, 128]
    }
  },
  email: {
    type: Sequelize.STRING(256),
    field: 'email',
    validate: {
      notEmpty: true,
    }
  },
  password: {
    type: Sequelize.VIRTUAL,
    validate: {
      notEmpty: true,
      len: [5, 48],
    }
  },
  passwordHash: {
    type: Sequelize.CHAR(60),
    field: 'password_hash',
  },
  passwordSalt: {
    type: Sequelize.CHAR(29),
    field: 'password_salt',
  },
  facebookId: {
    type: Sequelize.STRING(32),
    field: 'facebook_id',
  },
  vkontakteId: {
    type: Sequelize.STRING(32),
    field: 'vkontakte_id',
  },
  googleId: {
    type: Sequelize.STRING(32),
    field: 'vkontakte_id',
  },
  twitterId: {
    type: Sequelize.STRING(32),
    field: 'twitter_id',
  },
}, {
  tableName: 'user',
  underscored: true,
  timestamps:  false,

  validate: {
    withCredentials() {
      if (this.facebookId || this.googleId || this.vkontakteId || this.twitterId) {
        return true
      }

      const errors = {}

      if (_.isNil(this.email)) {
        errors['email'] = 'absent'
      }

      if (_.isNil(this.passwordHash)) {
        errors['password'] = 'absent'
      }

      if (!_.isEmpty(errors)) {
        throw errors
      }

      return true
    }
  }
})
