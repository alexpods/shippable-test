const express = require('express')
const auth = require('../middlewares/auth')

module.exports = express.Router()

  .get('/redirect/facebook',
    auth.redirectToFacebook
  )

  .get('/redirect/vkontakte',
    auth.redirectToVkontakte
  )

  .get('/redirect/google',
    auth.redirectToGoogle
  )

  .get('/redirect/twitter',
    auth.redirectToTwitter
  )

  .post('/tokens/local',
    auth.local,
    auth.returnToken
  )

  .post('/tokens/facebook',
    auth.facebook,
    auth.returnToken
  )

  .post('/tokens/vkontakte',
    auth.vkontakte,
    auth.returnToken
  )

  .post('/tokens/google',
    auth.google,
    auth.returnToken
  )

  .post('/tokens/twitter',
    auth.twitter,
    auth.returnToken
  )
