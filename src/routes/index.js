const express = require('express')

module.exports =  express.Router()
  .use('/auth',  require('./auth'))
  .use('/users', require('./users'))