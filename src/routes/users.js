const express = require('express')
const users = require('../middlewares/users')

module.exports = express.Router()

  .get('/:userId',
    users.get
  )

  .post('/',
    users.sanitize,
    users.validate,
    users.create
  )