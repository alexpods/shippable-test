const Passport = require('passport').Passport
const LocalStrategy = require('passport-local').Strategy
const FacebookStrategy = require('passport-facebook').Strategy
const VkontakteStrategy = require('passport-vkontakte').Strategy
const GoogleStrategy = require('passport-google-oauth2').Strategy
const TwitterStrategy = require('passport-twitter').Strategy
const users = require('./users')

function convertUserToSimpleObject(user) {
  return {
    id: user.id
  }
}

function createSocialNetworkHandler(socialNetworkName) {
  return ($1, $2, profile, verified) => Promise.resolve()
    .then(async () => {
      var user = await users.getBySocialNetworkId(socialNetworkName, profile.id)

      if (!user) {
        user = await users.createUsingSocialNetworkProfile(profile)
      }

      return verified(null, convertUserToSimpleObject(user))
    })
    .catch(verified)
}

const passport = new Passport()

passport.serializeUser((user, done) => {
  done(null, JSON.stringify(user))
})

passport.deserializeUser((data, done) => {
  donw(null, JSON.parse(data))
})

passport.use(new LocalStrategy((login, password, verified) => {
  return Promise.resolve()
    .then(async () => {
      const user = await users.getByLogin(login)

      if (!user) {
        return verified(null, false, `Specified user is not found`)
      }

      const isPasswordValid = await users.isPasswordValid(user, password)

      if (!isPasswordValid) {
        return verified(null, false, 'Incorrect password')
      }

      return verified(null, convertUserToSimpleObject(user))
    })
    .catch((verified))
}))

passport.use(new FacebookStrategy({
  clientID:     '1191983110894098',
  clientSecret: '6d77a79fa14a0e256ba721adf2430fb5',
  callbackURL:  'https://example.com/',
  state: true,
}, createSocialNetworkHandler('facebook')))

passport.use(new VkontakteStrategy({
  clientID:    '5787734',
  clientSecret:'pIFfdphRsrI8z9vrahx5',
  callbackURL: 'https://example.com',
  state: true,
}, createSocialNetworkHandler('vkontakte')))

passport.use(new GoogleStrategy({
  clientID:    '736966447562-u7t09mg6vt4mar63efgd7j3ofrk39j2l.apps.googleusercontent.com',
  clientSecret:'9i2lVP5dcD2fYVn1JaemUdwl',
  callbackURL: 'https://example.com',
  scope: ['profile'],
  state: true,
}, createSocialNetworkHandler('google')))

passport.use(new TwitterStrategy({
  consumerKey:    'syDlQcIWB8TP2GjBEIIinwa1J',
  consumerSecret: 'rbF87hHqsySEBVeJOPGR3JQCf156jz6Q3VCDc1wIAGOYm4sQDk',
  callbackURL: 'https://example.com',
}, createSocialNetworkHandler('twitter')))

module.exports = passport