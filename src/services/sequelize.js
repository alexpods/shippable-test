const Sequelize = require('sequelize')
const path = require('path')

module.exports = new Sequelize('tm', null, null, {
  dialect: 'sqlite',
  storage:  path.resolve(__dirname, '../../database.sqlite')
})
