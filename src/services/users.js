const bcrypt = require('bcrypt')
const User = require('../models/user')

const ROUNDS = 10

module.exports = exports =  {

  getById: async (userId) => {
    return User.findById(userId)
  },

  getByLogin: async (login) => {
    return User.findOne({
      where: {
        email: login
      }
    })
  },

  getBySocialNetworkId: async (socialNetworkName, socialNetworkId) => {
    var fieldName = socialNetworkName + 'Id'

    return User.findOne({
      where: {
        [fieldName]: socialNetworkId
      }
    })
  },

  createUsingSocialNetworkProfile: async (profile) => {
    const newUser = User.build({
      name:       profile.displayName,
      facebookId: profile.id,
    })

    await newUser.save()

    return newUser
  },

  createUsingCredentials: async (name, email, password) => {
    const newUser = User.build({
      name:  name,
      email: email,
    })

    await exports.assignPassword(newUser, password)
    await newUser.save()

    return newUser
  },

  assignPassword: async (user, password) => {
    const salt = await bcrypt.genSalt(ROUNDS)

    user.passwordSalt = salt
    user.passwordHash = await bcrypt.hash(password, salt)
  },

  isPasswordValid: async (user, password) => {
    const hash = await bcrypt.hash(password, user.passwordSalt)
    return user.passwordHash === hash
  },

  toData: (user) => {
    return {
      id:     user.id,
      name:   user.name,
      email:  user.email,
    }
  }
}