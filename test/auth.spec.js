const chai = require('chai')
const supertest = require('supertest')
const jsonwebtoken = require('jsonwebtoken')
const nightmare = require('nightmare')
const url = require('url')
const querystring = require('querystring')
const FacebookStrategy = require('passport-facebook')
const _ = require('lodash')

const app = require('../src/app')
const sequelize = require('../src/services/sequelize')

const expect = chai.expect
const request = supertest(app)

function describeOauthRedirect(oauthVersion, serviceName, host) {
  describe(`GET /auth/redirect/${serviceName}`, function() {
    this.timeout(20000)

    it(`should return a redirect to ${serviceName} oauth authentication`, async () => {
      const response = await request.get(`/auth/redirect/${serviceName}`)

      expect(response.status).to.equal(302)
      expect(response.headers['location']).to.contain(host)
    })

    it('should return a redirect event if user provids "code" query parameter', async () => {
      const response = await request.get(`/auth/redirect/${serviceName}?code=12341234`)

      expect(response.status).to.equal(302)
    })
  })
}

function describeOauthToken(oauthVersion, serviceName, crawler) {
  var cookies, data, params

  switch (oauthVersion) {
    case 1:
      params = ['oauth_token', 'oauth_verifier']
      break
    case 2:
      params = ['code', 'state']
      break
  }

  function testParameterIsNotProveded(parameterName) {
    const camelCasedParameterName = _.camelCase(parameterName)

    it(`should return 400 HTTP status code if user doesn't provide "${camelCasedParameterName}" at all`, async () => {
      const dataWithoutParameter = _.clone(data)

      delete dataWithoutParameter[camelCasedParameterName]

      const response = await request
        .post(`/auth/tokens/${serviceName}`)
        .set('Cookie', cookies)
        .send(dataWithoutParameter)

      await expect(response.status).to.equal(400)
    })
  }

  function testParameterIsIncorrect(parameterName) {
    const camelCasedParameterName = _.camelCase(parameterName)

    it(`should return 403 HTTP status code if user provides incorrect "${camelCasedParameterName}"`, async () => {
      const dataWithIncorrectParameter = _.clone(data)

      dataWithIncorrectParameter[camelCasedParameterName] = '123fasfdasdf'

      const response = await request
        .post(`/auth/tokens/${serviceName}`)
        .set('Cookie', cookies)
        .send(dataWithIncorrectParameter)

      expect(response.status).to.equal(403)
    })
  }

  xdescribe(`POST /auth/tokens/${serviceName}`, function() {
    this.timeout(20000)

    beforeEach(async () => {
      await sequelize.sync({ force: true })

      const redirectResponse = await request.get(`/auth/redirect/${serviceName}`)

      oauthUrl = redirectResponse.header['location']
      cookies  = redirectResponse.header['set-cookie'].map(cookie => cookie.split(';')[0]).join(';')

      const nm = nightmare({ typeInterval: 1 }).goto(oauthUrl)

      const redirectUrl = await crawler(nm)
        .wait(() => location.host === 'example.com')
        .url()
        .end()

      const query = querystring.parse(url.parse(redirectUrl).query)

      data = {}

      for (var param of params) {
        data[_.camelCase(param)] = query[param]
      }
    })

    it('should create and return a new JWT token', async () => {
      const authResponse = await request
        .post(`/auth/tokens/${serviceName}`)
        .set('Cookie', cookies)
        .send(data)

      expect(authResponse.status).to.equal(200)

      const token   = authResponse.body.token
      const payload = jsonwebtoken.verify(token, 'xxx')
      const userId  = payload.id

      const userResponse = await request.get(`/users/${userId}`)

      expect(userResponse.status).to.equal(200)
    })

    for (var param of params) {
      testParameterIsNotProveded(param)
      testParameterIsIncorrect(param)
    }
  })
}

describeOauthRedirect(2, 'facebook',  'facebook.com')
describeOauthRedirect(2, 'vkontakte', 'vk.com')
describeOauthRedirect(2, 'google',    'google.com')
describeOauthRedirect(1, 'twitter',   'twitter.com')

describeOauthToken(2, 'facebook', (nm) => {
  return nm
    .wait('#login_form')
    .type('#email', 'aleksey.podskrebyshev@gmail.com')
    .type('#pass',  'IseeDeadPeople')
    .click('[type=submit]')
})

describeOauthToken(2, 'vkontakte', (nm) => {
  return nm
    .wait('#login_submit')
    .type('input[name=email]', '+66945983445')
    .type('input[name=pass]',  '32167qwerty')
    .click('[type=submit]')
})

describeOauthToken(2, 'google', (nm) => {
  return nm
    .wait('#Email')
    .type('#Email', 'alexpods.test.user@gmail.com')
    .click('#next')
    .wait('#Passwd')
    .type('#Passwd', '32167qwerty')
    .click('#signIn')
})

describeOauthToken(1, 'twitter', (nm) => {
  return nm
    .wait('#oauth_form')
    .type('#username_or_email', 'alexpods.test.user@gmail.com')
    .type('#password', '32167qwerty')
    .click('#allow')
})

describe('POST /auth/tokens/local', () => {
  var userInfo

  beforeEach(async () => {
    await sequelize.sync({ force: true })

    const userData = {
      name: 'Alex',
      email: 'a@m.ru',
      password: '12345'
    }

    const response = await request.post('/users').send(userData)

    userInfo = Object.assign({}, response.body, {
      password: userData['password']
    })
  })

  it('should create and return a new JWT token', async () => {
    const authResponse = await request
      .post('/auth/tokens/local')
      .send({
        username: userInfo.email,
        password: userInfo.password,
      })

      expect(authResponse.status).to.equal(200)

      const token   = authResponse.body.token
      const payload = jsonwebtoken.verify(token, 'xxx')
      const userId  = payload.id

      const userResponse = await request.get(`/users/${userId}`)

      expect(userResponse.status).to.equal(200)
  })

  it('should return 401 status code if user provides bad credentials', async () => {
    const response = await request
      .post('/auth/tokens/local')
      .send({
        username: 'Absent user',
        password: 'xxxx',
      })

    expect(response.status).to.equal(401)
    expect(response.headers).to.have.property('www-authenticate')
  })
})
