const _ = require('lodash')
const chai = require('chai')
const supertest = require('supertest')
const sequelize = require('../src/services/sequelize')
const app = require('../src/app')
const testValidation = require('./utils').testValidation

const expect = chai.expect
const request = supertest(app)

const USER_DATA_FILEDS = ['name', 'email']

describe('/users', () => {

  beforeEach(async () => {
    await sequelize.sync({ force: true })
  })

  describe ('GET /:userId', () => {
    it('should return 404 status code if user with specified id doesn\'t exist', async () => {
      const response = await request.get('/users/999999999')
      expect(response.status).to.equal(404)
    })
  })

  describe('POST /', () => {
    var data

    beforeEach(() => {
      data = {
        name:    'Alex',
        email:   'a1@m.com',
        password:'12345'
      }
    })

    it('should create a new user', async () => {
      const createResponse = await request.post('/users').send(data)

      expect(createResponse.status).to.equal(200)
      expect(createResponse.body.id).to.not.be.empty

      for (var field of USER_DATA_FILEDS) {
        expect(createResponse.body).to.have.property(field, data[field])
      }

      const userId = createResponse.body.id

      const getResponse = await request.get(`/users/${userId}`).send()

      expect(getResponse.status).to.equal(200)
      expect(getResponse.body).to.deep.equal(createResponse.body)
    })

    testValidation(() => {
      return request.post('/users').send(data)
    }, {
      'name is not specified': ['name:empty', () => {
        delete data['name']
      }],
      'name is an empty string': ['name:empty', () => {
        data['name'] = ''
      }],
      'email is not specified': ['email:empty', () => {
        delete data['email']
      }],
      'email is an empty string': ['email:empty', () => {
        data['email'] = ''
      }],
      'password is not specified': ['password:empty', () => {
        delete data['password']
      }],
      'password is an empty string': ['password:empty', () => {
        data['password'] = ''
      }],
      'email is incorrect': ['email:incorrect_email', () => {
        data['email'] = 'asdfasdf'
      }]
    })

    it('should trim values of name and email', async () => {
      const updatedData = _.clone(data)

      updatedData.name  = '    ' + data.name + '    '
      updatedData.email = '    ' + data.email + '    '

      const response = await request.post('/users').send(updatedData)

      expect(response.body.name).to.equal(data.name)
      expect(response.body.email).to.equal(data.email)
    })
  })
})

