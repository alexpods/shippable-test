const expect = require('chai').expect

exports.testValidation = function (makeRequest, rules) {

  for (const description in rules) {
    it(`should return 422 HTTP status code if ${description}`, async () => {
      const expectedErrorsList = [].concat(rules[description][0]).sort()
      const prepareData = rules[description][1]

      prepareData()

      const response = await makeRequest()

      expect(response.status).to.equal(422)
      expect(response.body.errors).to.be.an('array')

      const actualErrorsList = response.body.errors
        .map((error) => `${error.param}:${error.msg}`)
        .sort()

      expect(actualErrorsList).to.deep.equal(expectedErrorsList)
    })
  }
}
